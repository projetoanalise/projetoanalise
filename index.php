<?php

/*
 * Iniciação da sessão, importação do arquivo com os dados de conexão com o
 * banco de dados e a definição da constante 'BASE_URL' que será usada na
 * construção de redirecionamentos e de importações de arquivos.
 *
 */
session_start();
require 'config.php';
define('BASE_URL', 'http://localhost/projetoanalise');


/*
 * Autoload versão psr-0.
 *
 * O autoload permite que não se precise incluir ou requerir a classe que será
 * instanciada uma vez que esse classe é importada dinamicamente quando usada
 * via autoload.
 *
 * OBS: Atualmente o padrão de autoload vigente é o psr-4, autoload com composer
 *      Porém para manter a simplicidade desse projeto o composer não foi usado,
        logo optou-se por utilizar a versão psr-0 para autoload.
 */
spl_autoload_register(function ($class)
{
    if      (file_exists('Controllers/'.$class.'.php')) {
            require_once 'Controllers/'.$class.'.php';
    } elseif(file_exists('Models/'.$class.'.php')) {
            require_once 'Models/'.$class.'.php';
    } elseif(file_exists('Core/'.$class.'.php')) {
            require_once 'Core/'.$class.'.php';
    }
});


/*
 * Instanciação da classe 'Core' e chamada do método 'run'.
 *
 * Esse método faz o partilhamento da url extraindo o controler, model e um id.
 * Ex: http://meusite/controller/model/id ==> facebook.com/abrir/photo/iddafoto.
 */
$core = new Core();
$core->run();
