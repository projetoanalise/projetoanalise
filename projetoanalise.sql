-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 05/11/2018 às 18:56
-- Versão do servidor: 10.1.35-MariaDB
-- Versão do PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetoanalise`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mitigacoes`
--

CREATE TABLE `mitigacoes` (
  `id_mitigacao` int(4) NOT NULL,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `mitigacoes`
--

INSERT INTO `mitigacoes` (`id_mitigacao`, `nome`, `descricao`) VALUES
(2, 'beubue buebue', 'asdasdasda '),
(3, 'asdadsad', 'asdjhaduhadshia\r\n'),
(4, 'test', 'afasdas\r\n\r\n'),
(5, 'asfasdasdtest2', 'asdasdasd');

-- --------------------------------------------------------

--
-- Estrutura da tabela `objetos`
--

CREATE TABLE `objetos` (
  `id_objeto` int(11) NOT NULL,
  `nome` varchar(15) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `id_processo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `objetos`
--

INSERT INTO `objetos` (`id_objeto`, `nome`, `descricao`, `id_processo`) VALUES
(1, 'Computador', 'PC dos caras', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `objeto_risco`
--

CREATE TABLE `objeto_risco` (
  `id_objeto` int(11) NOT NULL,
  `id_risco` int(11) NOT NULL,
  `avaliacao_mitigacao` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `objeto_risco`
--

INSERT INTO `objeto_risco` (`id_objeto`, `id_risco`, `avaliacao_mitigacao`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `objeto_risco_mitigacao`
--

CREATE TABLE `objeto_risco_mitigacao` (
  `id_objeto` int(11) NOT NULL,
  `id_risco` int(11) NOT NULL,
  `id_mitigacao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `objeto_risco_mitigacao`
--

INSERT INTO `objeto_risco_mitigacao` (`id_objeto`, `id_risco`, `id_mitigacao`) VALUES
(1, 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `processos`
--

CREATE TABLE `processos` (
  `id_processo` int(4) NOT NULL,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `processos`
--

INSERT INTO `processos` (`id_processo`, `nome`, `descricao`) VALUES
(1, 'Manter LabWeb', 'Manter os equipamentos do laboratório de web salvos e funcionando. ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `riscos`
--

CREATE TABLE `riscos` (
  `id_risco` int(4) NOT NULL,
  `impacto` int(2) NOT NULL,
  `probabilidade` int(2) NOT NULL,
  `nome` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descricao` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resultado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `riscos`
--

INSERT INTO `riscos` (`id_risco`, `impacto`, `probabilidade`, `nome`, `descricao`, `resultado`) VALUES
(1, 2, 3, 'Cair', 'A droga do computador cair na merda do chï¿½o, lek.   ', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(4) NOT NULL,
  `nome` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sobrenome` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `senha` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nome`, `sobrenome`, `email`, `senha`, `cargo`) VALUES
(2, 'NomeGestor', 'SobrenomeGestor', 'gestor@auditoria.com', '202cb962ac59075b964b07152d234b70', 'gestor'),
(3, 'NomeAuditor', 'SobrenomeAudito', 'auditor@auditoria.com', '202cb962ac59075b964b07152d234b70', 'auditor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mitigacoes`
--
ALTER TABLE `mitigacoes`
  ADD PRIMARY KEY (`id_mitigacao`);

--
-- Indexes for table `objetos`
--
ALTER TABLE `objetos`
  ADD PRIMARY KEY (`id_objeto`),
  ADD KEY `fk_ObjetoProcesso` (`id_processo`);

--
-- Indexes for table `objeto_risco`
--
ALTER TABLE `objeto_risco`
  ADD PRIMARY KEY (`id_objeto`,`id_risco`),
  ADD KEY `fk_riscos` (`id_risco`);
-- Índices de tabela `objeto_risco_mitigacao`
--
--
-- Índices de tabela `processos`
-- Indexes for table `objeto_risco_mitigacao`
--
ALTER TABLE `objeto_risco_mitigacao`
  ADD PRIMARY KEY (`id_objeto`,`id_risco`,`id_mitigacao`),
  ADD KEY `fk_risco` (`id_risco`),
  ADD KEY `fk_mitigacao` (`id_mitigacao`);

--
-- Indexes for table `processos`
--
ALTER TABLE `processos`
  ADD PRIMARY KEY (`id_processo`);

--
-- Indexes for table `riscos`
--
ALTER TABLE `riscos`
  ADD PRIMARY KEY (`id_risco`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mitigacoes`
--
ALTER TABLE `mitigacoes`
  MODIFY `id_mitigacao` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `objetos`
--
ALTER TABLE `objetos`
  MODIFY `id_objeto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `processos`
--
ALTER TABLE `processos`
  MODIFY `id_processo` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `riscos`
--
ALTER TABLE `riscos`
  MODIFY `id_risco` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `objetos`
--
ALTER TABLE `objetos`
  ADD CONSTRAINT `fk_ObjetoProcesso` FOREIGN KEY (`id_processo`) REFERENCES `processos` (`id_processo`);

--
-- Limitadores para a tabela `objeto_risco`
--
ALTER TABLE `objeto_risco`
  ADD CONSTRAINT `fk_objetorisco_objeto` FOREIGN KEY (`id_objeto`) REFERENCES `objetos` (`id_objeto`),
  ADD CONSTRAINT `fk_objetorisco_risco` FOREIGN KEY (`id_risco`) REFERENCES `riscos` (`id_risco`);


--
-- Restrições para tabelas `objeto_risco_mitigacao`
--
ALTER TABLE `objeto_risco_mitigacao`
  ADD CONSTRAINT `fk_orm_mitigacao` FOREIGN KEY (`id_mitigacao`) REFERENCES `mitigacoes` (`id_mitigacao`),
  ADD CONSTRAINT `fk_orm_objetorisco` FOREIGN KEY (`id_objeto`) REFERENCES `objeto_risco` (`id_objeto`),
  ADD CONSTRAINT `fk_orm_objetorisco_risco` FOREIGN KEY (`id_risco`) REFERENCES `objeto_risco` (`id_risco`),
  ADD CONSTRAINT `fk_objetos` FOREIGN KEY (`id_objeto`) REFERENCES `objetos` (`id_objeto`),
  ADD CONSTRAINT `fk_riscos` FOREIGN KEY (`id_risco`) REFERENCES `riscos` (`id_risco`);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
