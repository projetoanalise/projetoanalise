<?php

/*
 * Controller de Mitigações.
 *
 * #Verifica se o usuário está logado(se há uma sessão).
 * #index(lista as mitigações).
 * #Faz o logout.
 * #Inserir
 * #Editar
 * #Excluir
 * #Listar mitigações
 *
 */
class MitigacoesController extends Controller
{


  // Verifica se há uma sessão aberta, se não houver redireciona para o login.
  public function __construct()
  {
    parent::__construct(); // Estende o construtor da classe pai (Controller).
    $this->estaLogado();
  }


  // Chama a view 'mitigacoes' passando o vetor de dados '$data'.
  public function index()
  {
    $data = array();
    $data = $this->listaMitigacoes($data);
    $this->loadTemplate('mitigacoes', $data);
  }


  // Lista as mitigações.
  public function listaMitigacoes($mitigacoes = array())
  {
    $mitigacao = new Mitigacoes();
    $mitigacoes['lista_de_mitigacoes'] = $mitigacao->getMitigacoes($mitigacoes);
    return $mitigacoes;
  }


  // Insere uma mitigação com dados recebidos via POST.
  public function inserir()
  {
    $mitigacao = new Mitigacoes();
    if(isset($_POST['nome']) && !empty($_POST['nome'])) {
      $nome = addslashes($_POST['nome']);
      $descricao = addslashes($_POST['descricao']);
      $mitigacao->inserir($nome, $descricao);
      header("Location: ".BASE_URL."/mitigacoes");
    }
  }


  // Recebe o id de uma mitigação e edita o registro referente a esse id.
  public function editar($id_mitigacao)
  {
   $mitigacao = new Mitigacoes();
   if(isset($_POST['nome']) && !empty($_POST['nome'])) {
     $nome = addslashes($_POST['nome']);
     $descricao = addslashes($_POST['descricao']);

     $mitigacao->editar($id_mitigacao, $nome, $descricao);
     header("Location: ".BASE_URL."/mitigacoes");
   }
  }


  // Recebe o id de uma mitigação e exclui o registro referente a esse id.
  public function excluir($id_mitigacao)
  {
    $mitigacao = new Mitigacoes();
    $mitigacao->excluir($id_mitigacao);
    header("Location: ".BASE_URL."/mitigacoes");
  }

}
