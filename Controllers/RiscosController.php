<?php

/*
 * Controller de Mitigações.
 *
 * #Verifica se o usuário está logado(se há uma sessão).
 * #index(lista as mitigações).
 * #Faz o logout.
 * #Inserir
 * #Editar
 * #Excluir
 * #Listar mitigações
 *
 */
class RiscosController extends Controller
{


  // Verifica se há uma sessão aberta, se não houver redireciona para o login.
  public function __construct()
  {
    parent::__construct(); // Estende o construtor da classe pai (Controller).
    $this->estaLogado();
  }


  /*
   * Função chamada automaticamente.
   * Cria um array e usa-o para receber a lista de riscos da action
   * 'listaRiscos' que está ainda nesse controller. Depois chama o template
   * passando seu nome e os dados atualizados.
   */
  public function index()
  {
      $data = array();
      $data = $this->listarObjetos($data);
      $data = $this->listaRiscos($data);
      $data = $this->marcarObjetos($data);
      $data = $this->listaRiscos($data);
      $this->loadTemplate('riscos', $data);
  }


  //
  public function marcarObjetos($objetos_riscos = array())
  {
    $objetos_risco = new Objetos();
    $objetos_riscos['objeto_risco'] = $objetos_risco->listarObjetosRiscos($objetos_riscos);
    return $objetos_riscos;
  }


  //
  public function listarObjetos($objetos = array())
  {
    $objeto = new Objetos();
    $objetos['lista_de_objetos'] = $objeto->getObjetos($objetos);
    return $objetos;
  }


  //
  public function inserir()
  {
    $usuario = new Usuarios();
    $cargo = $usuario->temPermissao($_SESSION['id']);
    if($cargo === "gestor") {
      header("Location: ".BASE_URL);
    } else {
      $risco = new Riscos();
      if(isset($_POST['nome']) && !empty($_POST['nome'])) {
        $nome = addslashes($_POST['nome']);
        $descricao = addslashes($_POST['descricao']);
        $impacto = addslashes($_POST['impacto']);
        $probabilidade = addslashes($_POST['probabilidade']);
        $risco->inserir($nome, $descricao, $impacto, $probabilidade);
        header("Location: ".BASE_URL."/riscos");
      }
    }
  }


  //
  public function editar($id_risco)
  {
    $risco = new Riscos();
    if(isset($_POST['nome']) && !empty($_POST['nome'])) {
     $nome = addslashes($_POST['nome']);
     $descricao = addslashes($_POST['descricao']);
     $impacto = addslashes($_POST['impacto']);
     $probabilidade = addslashes($_POST['probabilidade']);
     $risco->editar($id_risco, $nome, $descricao, $impacto, $probabilidade);
     header("Location: ".BASE_URL."/riscos");
    }
  }


  //
  public function associar($id_risco)
  {
    $objetorisco = new Riscos();
    foreach($_POST["checkbox"] as $id_objeto){
      $objetos_riscos = $objetorisco->associarObjetosRiscos($id_objeto, $id_risco);
    }
    header("Location: ".BASE_URL."/riscos");
  }


  //
  public function excluir($id_risco)
  {
    $risco = new Riscos();
    $risco->excluir($id_risco);
    header("Location: ".BASE_URL."/riscos");
  }


  //
  public function listaRiscos($riscos = array())
  {
    $risco = new Riscos();
    $riscos['lista_de_riscos'] = $risco->listarRiscos($riscos);
    return $riscos;
  }


  //
  public function desassociar($id_risco)
  {
    $objetorisco = new Riscos();
    foreach($_POST["checkbox"] as $id_objeto){
      $objetos_riscos = $objetorisco->excluirObjetoRisco($id_objeto, $id_risco);
    }
    header("Location: ".BASE_URL."/riscos");
  }

}
