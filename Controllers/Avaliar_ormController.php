<?php

/*
 * Controller de Mitigações.
 *
 * #Verifica se o usuário está logado(se há uma sessão).
 * #index(lista as mitigações).
 * #Faz o logout.
 * #Inserir
 * #Editar
 * #Excluir
 * #Listar mitigações
 *
 */
class Avaliar_ormController extends Controller
{


  // Verifica se há uma sessão aberta, se não houver redireciona para o login.
  public function __construct()
  {
    $this->estaLogado();
  }


  /*
   *
   *
   */
  public function index()
  {
      $data = array();
      $data = $this->listarObjetosRiscos($data);
      $data = $this->listaMitigacoes($data);
      $data = $this->listarObjetos($data);
      $data = $this->listaRiscos($data);
      $data = $this->listarObjetosRiscosMitigacoes($data);
      $this->loadTemplate('avaliar_orm', $data);
  }


  //
  public function listarObjetosRiscosMitigacoes($ObjetoRiscoMitigacao = array())
  {
    $ORM = new ObjetosRiscosMitigacoes();
    $ObjetoRiscoMitigacao['ObjetoRiscoMitigacao'] =
    $ORM->getObjetosRiscosMitigacoes($ObjetoRiscoMitigacao);
    return $ObjetoRiscoMitigacao;
  }


  //
  public function listaMitigacoes($mitigacoes = array())
  {
    $mitigacao = new Mitigacoes();
    $mitigacoes['lista_de_mitigacoes'] = $mitigacao->getMitigacoes($mitigacoes);
    return $mitigacoes;
  }


  //
  public function listarObjetos($objetos = array())
  {
    $objeto = new Objetos();
    $objetos['lista_de_objetos'] = $objeto->getObjetos($objetos);
    return $objetos;
  }


  //
  public function listaRiscos($riscos = array())
  {
    $risco = new Riscos();
    $riscos['lista_de_riscos'] = $risco->listarRiscos($riscos);
    return $riscos;
  }


  //
  public function listarObjetosRiscos($objetos = array())
  {
    $objetorisco = new ObjetosRiscosMitigacoes();
    $objetosriscos['lista_de_objetos_riscos'] = $objetorisco->listarObjetosRiscos($objetos);
    return $objetosriscos;
  }


  //
  public function avaliar()
  {
    $orm = new ObjetosRiscosMitigacoes();
    if(isset($_POST['classificacao']) && !empty($_POST['classificacao'])) {
      $classificacao = addslashes($_POST['classificacao']);
      $risco = addslashes($_POST['risco']);
      $objeto = addslashes($_POST['objeto']);
      $x = $orm->avaliar($classificacao, $risco, $objeto);
          header("Location: ".BASE_URL."/Avaliar_orm");
    }
  }


  //
  public function excluir()
  {
    $orm = new ObjetosRiscosMitigacoes();
    $risco = addslashes($_POST['risco']);
    $objeto = addslashes($_POST['objeto']);
    $x = $orm->excluirObjetoRisco($risco, $objeto);
    header("Location: ".BASE_URL."/Avaliar_orm");
  }


  //
  public function associar($id_risco)
  {
    $ORM = new ObjetosRiscosMitigacoes();
    $risco = addslashes($_POST['risco']);
    $objeto = addslashes($_POST['objeto']);

    foreach($_POST["checkbox"] as $id_mitigacao){
      $objetos_riscos = $ORM->associarObjetosRiscosMitigacao($objeto, $risco,
                                                             $id_mitigacao);
    }
    header("Location: ".BASE_URL."/Avaliar_orm");
  }


  //
  public function desassociar($id_risco)
  {
    $ORM = new ObjetosRiscosMitigacoes();
    $risco = addslashes($_POST['risco']);
    $objeto = addslashes($_POST['objeto']);

    foreach($_POST["checkbox"] as $id_mitigacao){
      $objetos_riscos = $ORM->desassociarObjetosRiscosMitigacao($objeto, $risco,
                                                                $id_mitigacao);
    }
      header("Location: ".BASE_URL."/Avaliar_orm");
    }

}
