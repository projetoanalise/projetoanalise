<?php

/*
 * Controller de Mitigações.
 *
 * #Verifica se o usuário está logado(se há uma sessão).
 * #index(lista as mitigações).
 * #Faz o logout.
 * #Inserir
 * #Editar
 * #Excluir
 * #Listar mitigações
 *
 */
class ProcessosController extends Controller
{

  // Verifica se há uma sessão aberta, se não houver redireciona para o login.
  public function __construct()
  {
    parent::__construct(); // Estende o construtor da classe pai (Controller).
    $this->estaLogado();
    $usuario = new Usuarios();
    $cargo = $usuario->temPermissao($_SESSION['id']);
    if($cargo === "auditor") {
      header("Location: ".BASE_URL);
    }
  }


  /*
   * Chama a action 'listarProcessos' passando o vetor de dados '$data' que
   * depois é enviado para a view.
   */
  public function index()
  {
    $data = array();
    $data = $this->listarProcessos($data);
    $data = $this->listarObjetos($data);
    $this->loadTemplate('processos', $data);
  }


  /*
   * Instancia a classe 'Processos' e guarda o resultado retornado pela action
   * 'getProcessos' no vetor '$processos', depois retorna esse vetor.
   */
   public function listarProcessos($processos = array())
   {
     $processo = new Processos();
     $processos['lista_de_processos'] = $processo->getProcessos($processos);
     return $processos;
   }


   /*
    * Instacia o model 'Objetos', recebe um array onde serão guardados os
    * objetos. Depois faz uma requisição ao model 'Objetos' por todos os objetos
    e salva no vetor. Depois retorna esse vetor.
    *
    */
    public function listarObjetos($objetos = array())
    {
      $objeto = new Objetos();
      $objetos['lista_de_objetos'] = $objeto->getObjetos($objetos);
      return $objetos;
    }


   /*
    * Instacia o model 'Objetos', verifica se pelo menos o nome está setado no
    * post. Salva esses valores um variáveis e lança-as como parâmetro na
    * chamada de action 'novoObjeto'. Depois redireciona o usuario para a tela
    * de processos.
    *
    */
   public function associarNovoObjeto($id_processo)
   {
     $objeto = new Objetos();
     if(isset($_POST['nome']) && !empty($_POST['nome'])) {
       $nome = addslashes($_POST['nome']);
       $descricao = addslashes($_POST['descricao']);
       $objeto->novoObjeto($nome, $descricao, $id_processo);
       header("Location: ".BASE_URL."/processos");
     }
   }


   /*
    * Instancia o model 'Processos', verifica se pelo menos o nome foi setado
    * via post, salva esses valores em variáveis e manda estas como parâmetro
    * na chamada de action 'inserir'. Depois redireciona o usuário para a tela
    * de processos.
    *
    */
    public function inserir()
    {
      $processo = new Processos();
      if(isset($_POST['nome']) && !empty($_POST['nome'])) {
        $nome = addslashes($_POST['nome']);
        $descricao = addslashes($_POST['descricao']);
        $processo->inserir($nome, $descricao);
        header("Location: ".BASE_URL."/processos");
      }
    }


    /*
     * Instancia o model 'Processos', verifica se pelo menos o nome foi setado
     * via post, salva esses valores em variáveis e passa estas como parâmetros
     * na chamada de action 'editar'. Depois redireciona o usuário para a tela
     * de processos.
     *
     */
     public function editar($id_processo)
     {
       $processo = new Processos();
       if(isset($_POST['nome']) && !empty($_POST['nome'])) {
         $nome = addslashes($_POST['nome']);
         $descricao = addslashes($_POST['descricao']);
         $processo->editar($id_processo, $nome, $descricao);
         header("Location: ".BASE_URL."/processos");
       }

     }


     /*
      * Apenas instancia o model 'Processos' e chamada a action 'excluir'
      * passando o mesmo parâmetro que recebe da view: o id do processo. Depois
      * redireciona o usuário para a tela de processos.
      *
      */
      public function excluir($id_processo)
      {
        $processo = new Processos();
        $processo->excluir($id_processo);
        header("Location: ".BASE_URL."/processos");
      }

}
