<?php

/*
 * Controller do login.
 *
 * #Index(faz o login).
 * #Faz o logout.
 *
 */
class LoginController extends Controller
{

  /*
   * Verifica se o usuário digitou pelo menos o email, salva os dados (email e
   * senha) nas variáveis com mesmo nome, instancia a classe 'Usuarios' e,
   * utilizando a action 'login' faz a autenticação, se os dados estiverem
   * corretos o usuário é redirecionado para a página home, se não uma mensagem
   * de erro é mostrada.
   *
   */
  public function index()
  {
    $data = array();
    if(isset($_POST['email']) && !empty($_POST['email'])){
      $email = addslashes($_POST['email']);
      $senha = addslashes($_POST['senha']);

      $u = new Usuarios();

      if($u->login($email, $senha)){
        header("Location: ".BASE_URL);
      }else {
        $data['error'] = 'Ops, algo deu errado!!!';
      }
    }
    $this->loadView('login', $data);
  }


  /*
   * Instancia a classe 'Usuarios' e chama a action 'logout' inclusa nesta.
   * Logo depois redireciona o usuário para a raíz do sistema, que, uma vez que
   * a sessão foi finalizada, será a página de login.
   *
   */
  public function logout()
  {
    $usuario = new Usuarios();
    $usuario->logout();
    header("Location: ".BASE_URL);
  }

}
