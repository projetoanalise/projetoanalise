<?php

/*
 * Controller da home.
 *
 * #Verifica se há uma sessão aberta(construtor).
 * #Index.
 *
 */
class UsuarioController extends Controller
{

  // Verifica se há uma sessão aberta, se não houver redireciona para o login.
  public function __construct()
  {
    parent::__construct(); // Estende o construtor da classe pai (Controller).
    $this->estaLogado();
  }


  //
  public function index()
  {
  }

}
