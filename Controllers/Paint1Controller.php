<?php

/*
 * Controller de Mitigações.
 *
 * #Verifica se o usuário está logado(se há uma sessão).
 * #index(lista as mitigações).
 * #Faz o logout.
 * #Inserir
 * #Editar
 * #Excluir
 * #Listar mitigações
 *
 */
class Paint1Controller extends Controller
{


  // Verifica se há uma sessão aberta, se não houver redireciona para o login.
  public function __construct()
  {
    parent::__construct(); // Estende o construtor da classe pai (Controller).
    $this->estaLogado();
  }


  // Chama a view 'paint1' passando o vetor de dados '$data'.
  public function index()
  {
      $data = array();
      $data = $this->listaRiscos($data);


      $this->loadTemplate('paint1', $data);
  }


  public function listaRiscos($riscos = array()){
    $risco = new Riscos();
    $riscos['lista_de_riscos'] = $risco->listarRiscos($riscos);
    return $riscos;

  }
  
}
