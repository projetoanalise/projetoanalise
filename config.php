<?php
// Importa o arquivo que dita se estamos em desenvolvimento ou produção.
require 'environment.php';

/* Guarda os valores de acesso ao banco de dados em uma variável global,
	 disponível em todo o sistema. */
global $config;
$config = array();

// Dados de conexão ao banco de dados local e ao banco de dados remoto.
if(ENVIRONMENT == 'development') {
	$config['dbname'] = 'projetoanalise';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'root';
	$config['dbpass'] = '';
} else {
	$config['dbname'] = '#';
	$config['host'] = '#';
	$config['dbuser'] = '#';
	$config['dbpass'] = '#';
}
