<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Riscos</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#novorisco" data-whatever="@mdo">Novo Risco</button>

      <div class="modal fade" id="novorisco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cadastrar Novo Risco</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="<?php echo BASE_URL; ?>/riscos/inserir">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nome do Risco:</label>
                  <input name="nome" type="textarea"  class="form-control" id="recipient-name">
                </div>

                <label for="recipient-name" class="col-form-label">Probabilidade do Risco:</label>
                <select name="probabilidade" class="form-control" id="exampleFormControlSelect1">
                  <option value="1" >Baixo</option>
                  <option value="2" >Médio</option>
                  <option value="3" >Alto</option>
                </select>

              <div class="form-group">
                <label for="recipient-name" class="col-form-label">Impacto do Risco:</label>
                <select name="impacto" class="form-control" id="exampleFormControlSelect1">
                  <option value="1"  >Baixo</option>
                  <option value="2"  >Médio</option>
                  <option value="3"  >Alto</option>
                </select>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Descrição:</label>
                  <textarea name="descricao"  type="text" class="form-control" id="message-text"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  <button type="submit" class="btn btn-primary">Finalizar</button>
                </div></div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <br>

  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome do risco</th>
        <th scope="col">Descrição</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($lista_de_riscos as $risco): ?>
        <tr>
          <th scope="row"><?php echo $risco['id_risco']; ?></th>
          <td><?php echo $risco['nome']; ?></td>
          <td><?php echo $risco['descricao']; ?></td>
          <td>
            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#verobjetos<?php echo $risco['id_risco']; ?>">Associar Objetos</button>
            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#editarrisco<?php echo $risco['id_risco']; ?>">Editar</button>
            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#desassociar<?php echo $risco['id_risco']; ?>">Desassociar</button>

            <a href="<?php echo BASE_URL; ?>/riscos/excluir/<?php echo $risco['id_risco']; ?>"
              onclick="return confirm('Tem certeza que deseja excluir esse risco?')">
              <div class="btn btn-outline-primary btn-sm">
                  Excluir
              </div>
            </a>
          </td>

          <!-- Ver objetos -->
          <div class="modal fade" id="verobjetos<?php echo $risco['id_risco']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Associar objetos a '<?php echo $risco['nome']; ?>'</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/riscos/associar/<?php echo $risco['id_risco']; ?>">
                  <?php foreach ($lista_de_objetos as $objeto): ?>

                        <input type='checkbox' name="checkbox[]" id="<?php echo $objeto['id_objeto']; ?>" value="<?php echo $objeto['id_objeto']; ?>"
                        <?php foreach ($objeto_risco as $objetorisco):
                        if($objeto['id_objeto'] == $objetorisco['id_objeto'] && $risco['id_risco'] == $objetorisco['id_risco']){
                          echo 'checked="checked"';
                        }
                        endforeach; ?>
                        > <?php echo $objeto['nome']; ?> </input></br>
                        <?php endforeach; ?>

                </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Desassociar -->
          <div class="modal fade" id="desassociar<?php echo $risco['id_risco']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Desligar objetos de '<?php echo $risco['nome']; ?>'</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/riscos/desassociar/<?php echo $risco['id_risco']; ?>">
                    <?php foreach ($lista_de_objetos as $objeto):
                           foreach ($objeto_risco as $objetorisco):
                              if($objeto['id_objeto'] == $objetorisco['id_objeto'] && $risco['id_risco'] == $objetorisco['id_risco']){?>
                          <input type='checkbox' name="checkbox[]" id="<?php echo $objeto['id_objeto']; ?>" value="<?php echo $objeto['id_objeto']; ?>"
                            > <?php echo $objeto['nome']; ?> </input></br>
                              <?php }endforeach; ?>
                          <?php endforeach; ?>
                </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Editar Risco -->
          <div class="modal fade" id="editarrisco<?php echo $risco['id_risco']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Editar Risco</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/riscos/editar/<?php echo $risco['id_risco']; ?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Risco:</label>
                      <input name="nome" type="text" value="<?php echo $risco['nome']; ?>" class="form-control" id="recipient-name">
                    </div>

                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">ID do Risco:</label>
                      <input name="id_risco" type="text" disabled="disabled" value=<?php echo $risco['id_risco']; ?> class="form-control" id="recipient-id">
                    </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Probabilidade do Risco:</label>
                      <select name="probabilidade" class="form-control" id="exampleFormControlSelect1">
                        <option value="1" <?php echo $risco['probabilidade']=='1'?'selected':'';?> >Baixo</option>
                        <option value="2" <?php echo $risco['probabilidade']=='2'?'selected':'';?> >Médio</option>
                        <option value="3" <?php echo $risco['probabilidade']=='3'?'selected':'';?> >Alto</option>
                      </select>
                      </div>
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Impacto do Risco:</label>
                      <select name="impacto" class="form-control" id="exampleFormControlSelect1">
                        <option value="1" <?php echo $risco['impacto']=='1'?'selected':'';?> >Baixo</option>
                        <option value="2" <?php echo $risco['impacto']=='2'?'selected':'';?> >Médio</option>
                        <option value="3" <?php echo $risco['impacto']=='3'?'selected':'';?> >Alto</option>
                      </select>

                    </div>

                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descrição:</label>
                      <textarea name="descricao"  type="text" class="form-control" id="message-text"> <?php echo $risco["descricao"]; ?> </textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
  </main>
