<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link href="<?php echo BASE_URL; ?>/Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/Assets/css/template.css" rel="stylesheet">
    <link href="<?php echo BASE_URL; ?>/Assets/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo BASE_URL; ?>/Assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>/Assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>/Assets/js/script.js"></script>
    <link rel="icon" href="<?php echo BASE_URL; ?>/Assets/img/auditoria.png">
    <title>Auditoria</title>
  </head>

  <body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="<?php echo BASE_URL; ?>">
        <?php
          $usuario = new Usuarios();
          $cargo = $usuario->temPermissao($_SESSION['id']);
         ?>
        Controle de Auditoria | <?php echo $cargo; ?>
      </a>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo BASE_URL; ?>/home">
                  <span data-feather="home"></span>
                  Home
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo BASE_URL; ?>/paint1">
                  <span data-feather="map"></span>
                  Paint 1
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo BASE_URL; ?>/paint2">
                  <span data-feather="map"></span>
                  Paint 2
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo BASE_URL; ?>/processos">
                  <span data-feather="aperture"></span>
                  Processos
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo BASE_URL; ?>/riscos">
                  <span data-feather="alert-triangle"></span>
                  Riscos
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo BASE_URL; ?>/mitigacoes">
                  <span data-feather="activity"></span>
                  Mitigações
                </a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="<?php echo BASE_URL; ?>/avaliar_orm">
                <span data-feather="aperture"></span>
                Avaliar Mitigações
              </a>
            </li>
              <li class="nav-item">
              <a class="nav-link" href="<?php echo BASE_URL; ?>/login/logout">
                <span data-feather="aperture"></span>
                Sair
              </a>
            </li>
            </ul>

          </div>
        </nav>

        <?php $this->loadViewInTemplate($viewName, $viewData); ?>

      </div>

    </div>

    <!-- Icones -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

  </body>
</html>
