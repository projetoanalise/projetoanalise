<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Paint 2</h1>
  </div>

<div class="container" id='paint2'>
  <div class="row align-items-start" id='linha1p2'>
    <div class="col-1" id='l1c1p2'>
      R <BR>
      a <BR>
      n <BR>
      k <BR>
      i <BR>
      n <BR>
      g <BR>
    </div>
    <div class="col-1" id='l1c2p2'>
      A <br>
      l <br>
      t <br>
      o <br>
    </div>
    <div class="col" id='l1c3p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 5 || $risco['resultado'] == 6) && ($mitigacao['avaliacao_mitigacao']== 3)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
        ?>
      </div>
    <div class="col" id='l1c4p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 5 || $risco['resultado'] == 6) && ($mitigacao['avaliacao_mitigacao']== 2)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
        ?>
    </div>
    <div class="col" id='l1c5p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 5 || $risco['resultado'] == 6) && ($mitigacao['avaliacao_mitigacao']== 1)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
        ?>
      </div>
  </div>
  <div class="row align-items-center" id='linha2p2'>
    <div class="col-1" id='l2c1p2'>
        <br>
      d <br>
      o <br>
        <br>
      r <br>
      i <br>
      s <br>
      c <br>
      o <br>

    </div>
    <div class="col-1" id='l2c2p2'>
      M <BR>
      é <BR>
      d <BR>
      i <BR>
      o <BR>
    </div>
    <div class="col" id='l2c3p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 4) && ($mitigacao['avaliacao_mitigacao']== 3)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
      ?>
    </div>
    <div class="col" id='l2c4p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 4) && ($mitigacao['avaliacao_mitigacao']== 2)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
      ?>
    </div>
    <div class="col" id='l2c5p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 4) && ($mitigacao['avaliacao_mitigacao']== 1)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
      ?>
    </div>
  </div>
  <div class="row align-items-end" id='linha3p2'>
    <div class="col-1" id='l3c1p2'>
    </div>
    <div class="col-1" id='l3c2p2'>
      B <BR>
      a <BR>
      i <BR>
      x <BR>
      o <BR>
    </div>
    <div class="col" id='l3c3p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']){
            if (($risco['resultado'] == 2 || $risco['resultado'] == 3) && ($mitigacao['avaliacao_mitigacao']== 3)) {
              echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';

        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco (da variável risco) seja igual a id do risco (da variável mitigação) e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
       }}}
      }
      ?>
    </div>
    <div class="col" id='l3c4p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']) {
            if (($risco['resultado'] == 2 || $risco['resultado'] == 3) && ($mitigacao['avaliacao_mitigacao']== 2)) {
             echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
      ?>
    </div>
    <div class="col" id='l3c5p2'>
      <?php foreach ($lista_de_riscos as $risco) {
        foreach ($objeto_risco as $mitigacao) {
          if ($risco['id_risco'] == $mitigacao['id_risco']) {
            if (($risco['resultado'] == 2 || $risco['resultado'] == 3) && ($mitigacao['avaliacao_mitigacao']== 1)) {
              echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}}
        }
        //foreach pega a lista de riscos e salva na variável riscos, para depois pegar a tabela de objeto-risco e salvar na variável mitigação; caso a id do risco seja igual a id do objeto e caso o risco e a mitigação sejam da posição do quadrante, ele printa o conjunto no quadrante correspondente.
      ?>

    </div>
  </div>
  <div class="row align-items-end" id='linha4p2'>
    <div class="col-1" id='l4c1p2'>
    </div>
    <div class="col-1" id='l4c2p2'>

    </div>
    <div class="col" id='l4c3p2'>
      Forte
    </div>
    <div class="col" id='l4c4p2'>
      Parcialmente satisfatório
    </div>
    <div class="col" id='l4c5p2'>
      Fraco
    </div>
  </div>
  <div class="row align-items-end" id='linha5p2'>
    <div class="col-1" id='l5c1p2'>

    </div>
    <div class="col-1" id='l5c2p2'>

    </div>
    <div class="col" id='l5c3p2'>

    </div>
    <div class="col" id='l5c4p2'>
      <br>
      Eficácia do controle
    </div>
    <div class="col" id='l5c5p2'>
    </div>
  </div>
  </div>
</div>

  <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
</main>
