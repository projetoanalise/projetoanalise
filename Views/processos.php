<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Processos</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalNovoProcesso">Novo Processo</button>

      <div class="modal fade" id="modalNovoProcesso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cadastrar Novo Processo</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="<?php echo BASE_URL; ?>/processos/inserir">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nome do Processo:</label>
                  <input name="nome" type="text" class="form-control" id="recipient-name">
                </div>
                <div class="form-group">
                  <label for="message-text" class="col-form-label">Descrição:</label>
                  <textarea name="descricao" class="form-control" class=""></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  <button type="submit" class="btn btn-primary">Finalizar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <br>

<table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nome do Processo</th>
      <th scope="col">Descrição</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($lista_de_processos as $processo): ?>
      <tr>
        <th style="overflow: auto; width: 15px;" scope="row"><?php echo $processo['id_processo']; ?></th>
        <td style="overflow: auto; width: 160px;"><?php echo $processo['nome']; ?></td>
        <td style="overflow: auto; width: 470px;" ><?php echo $processo['descricao']; ?></td>
        <td>

          <!-- Associar Novo Objeto -->
          <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#modalAssociarObjeto_<?php echo $processo['id_processo']; ?>"> Associar Novo Objeto </button>
          <div class="modal fade" id="modalAssociarObjeto_<?php echo $processo['id_processo']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Associar Objeto a Este Processo</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/processos/associarNovoObjeto/<?php echo $processo['id_processo']; ?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Objeto:</label>
                      <input name="nome" type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descrição:</label>
                      <textarea style="height: 130px;" name="descricao" class="form-control" id="message-text"></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Ver Objetos -->
          <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#modalVerObjetos_<?php echo $processo['id_processo']; ?>"> Ver Objetos </button>
          <div class="modal fade" id="modalVerObjetos_<?php echo $processo['id_processo']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Objetos Associados a '<?php echo $processo['nome']; ?>'</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table class="table">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nome do Objeto</th>
                        <th scope="col">Descrição</th>
                        <th scope="col">Ações</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($lista_de_objetos as $objeto): ?>
                        <tr>
                        <?php if($objeto['id_processo'] == $processo['id_processo']): ?>

                            <th style="width: 20px;" scope="row"><?php echo $objeto['id_objeto']; ?></th>
                            <td style="width: 50px;"><?php echo $objeto['nome']; ?></td>
                            <td style="width: 360px;">
                              <div  style="width: 100%; height: 100%;">
                                <p>
                                  <?php echo $objeto['descricao']; ?>
                                </p>
                              </div>
                            </td>
                            <td style="width: 118px;">
                              <button type="button" class="btn btn-primary btn-sm">Editar</button>
                              <button type="button" class="btn btn-primary btn-sm">Excluir</button>
                            </td>

                        <?php endif; ?>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
              </div>
            </div>
          </div>

          <!-- Editar -->
          <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#modalEditar_<?php echo $processo['id_processo']; ?>"> Editar </button>
          <div class="modal fade" id="modalEditar_<?php echo $processo['id_processo']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Editar Processo</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/processos/editar/<?php echo $processo['id_processo']; ?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Processo:</label>
                      <input value="<?php echo $processo['nome']; ?>" name="nome" type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descrição:</label>
                      <textarea style="height: 130px;" name="descricao" class="form-control" id="message-text"><?php echo $processo['descricao']; ?></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <!-- Excluir -->
          <a href="<?php echo BASE_URL; ?>/processos/excluir/<?php echo $processo['id_processo']; ?>"
             onclick="return confirm('Tem certeza que deseja excluir esse processo?')">
            <div class="btn btn-outline-primary btn-sm">
              Excluir
            </div>
          </a>

        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>



  <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
</main>
