<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Mitigaçoes</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#novamitigacao" data-whatever="@mdo">Nova mitigação</button>

      <div class="modal fade" id="novamitigacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Cadastrar nova mitigação</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="<?php echo BASE_URL; ?>/mitigacoes/inserir">
                <div class="form-group">
                  <label for="recipient-name" class="col-form-label">Nome da Mitigação:</label>
                  <input name="nome" type="textarea"  class="form-control" id="recipient-name">
                </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Descrição:</label>
                  <textarea name="descricao"  type="text" class="form-control" id="message-text"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                  <button type="submit" class="btn btn-primary">Finalizar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <br>

  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome da mitigação</th>
        <th scope="col">Descrição</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($lista_de_mitigacoes as $mitigacao): ?>
        <tr>
          <th scope="row"><?php echo $mitigacao['id_mitigacao']; ?></th>
          <td><?php echo $mitigacao['nome']; ?></td>
          <td><?php echo $mitigacao['descricao']; ?></td>
          <td>
            <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#editarmitigacao<?php echo $mitigacao['id_mitigacao'] ?>">Editar</button>
            <a href="<?php echo BASE_URL; ?>/mitigacoes/excluir/<?php echo $mitigacao['id_mitigacao']; ?>"
               onclick="return confirm('Tem certeza que deseja excluir esse mitigacao?')">
              <div class="btn btn-outline-primary btn-sm">
                Excluir
              </div>
            </a>
          </td>

          <div class="modal fade" id="editarmitigacao<?php echo $mitigacao['id_mitigacao']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Editar Mitigação</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/mitigacoes/editar/<?php echo $mitigacao['id_mitigacao']; ?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Mitigação:</label>
                      <input name="nome" type="textarea" value=<?php echo $mitigacao["nome"]; ?> class="form-control" id="recipient-name">
                    </div>

                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Descrição:</label>
                      <textarea name="descricao"  type="text" class="form-control" id="message-text"><?php echo $mitigacao["descricao"]; ?></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
  </main>
