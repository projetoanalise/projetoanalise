<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Avalição das Mitigaçoes</h1>
    <div class="btn-toolbar mb-2 mb-md-0">
    </div>
  </div>

  <br>

  <table class="table table-striped table-bordered">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Nome do Objeto</th>
        <th scope="col">Nome do Risco</th>
        <th scope="col">Avaliação Atual</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($lista_de_objetos_riscos as $objetorisco): ?>
        <tr>
          <?php foreach ($lista_de_objetos as $objeto): ?>
            <?php if ($objetorisco['id_objeto'] == $objeto['id_objeto']): ?>
              <td><?php echo $objeto['nome']; ?></td>
            <?php endif; ?>
          <?php endforeach; ?>
          <?php foreach ($lista_de_riscos as $risco): ?>
            <?php if ($objetorisco['id_risco'] == $risco['id_risco']): ?>
              <th scope="row"><?php echo $risco['nome']; ?></th>
            <?php endif; ?>
          <?php endforeach; ?>
              <td><?php echo $objetorisco['avaliacao_mitigacao']; ?></td>

          <td>
            <form class="" action="<?php echo BASE_URL; ?>/ObjetosRiscosMitigacoes/excluir/<?php echo $mitigacao['id_mitigacao']; ?>" method="post">
              <input TYPE="hidden" NAME="objeto" VALUE="<?php echo $objetorisco['id_objeto']?>">
              <INPUT TYPE="hidden" NAME="risco" VALUE="<?php echo $objetorisco['id_risco']?>">
              <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#associar<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>">Associar</button>
              <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#avaliar<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>">Avaliar</button>
              <button type="button" class="btn btn-outline-primary btn-sm" data-toggle="modal"  data-target="#desassociar<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>">Desassociar</button>
              <button type="submit" class="btn btn-outline-primary btn-sm"
              onclick="return confirm('Tem certeza que deseja excluir esse objeto/risco?')">Excluir</button>
            </form>
          </td>


          <div class="modal fade" id="avaliar<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Avaliar Mitigações</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/ObjetosRiscosMitigacoes/avaliar/<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Objeto/Risco:</label>
                      <?php foreach ($lista_de_objetos as $objeto): ?>
                          <?php foreach ($lista_de_riscos as $risco): ?>
                            <?php if (($objetorisco['id_objeto'] == $objeto['id_objeto']) && ($objetorisco['id_risco'] == $risco['id_risco'])) : ?>
                                <input name="nome" type="textarea" value="<?php echo $objeto["nome"]; ?>/<?php echo $risco["nome"]; ?>" class="form-control" id="recipient-name">
                                  <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Mitigaçoes:</label>

                      <?php foreach ($ObjetoRiscoMitigacao as $ORM): ?>
                        <?php if (($objetorisco['id_objeto'] == $ORM['id_objeto']) && ($objetorisco['id_risco'] == $ORM['id_risco'])): ?>
                          <?php foreach ($lista_de_mitigacoes as $mitigacao): ?>
                            <?php if ($mitigacao['id_mitigacao'] == $ORM['id_mitigacao']) : ?>
                              <textarea name="descricao" disabled="disabled" type="text" class="form-control" id="message-text"><?php echo $mitigacao["descricao"] ?> </textarea>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </div>
                    <label for="recipient-name" class="col-form-label">Classificação das Mitigaçoes:</label>
                    <select name="classificacao" class="form-control" id="exampleFormControlSelect1">
                      <option value="1" >Baixo</option>
                      <option value="2" >Médio</option>
                      <option value="3" >Alto</option>
                    </select>
                    <INPUT TYPE="hidden" NAME="objeto" VALUE="<?php echo $objetorisco['id_objeto']?>"><INPUT TYPE="hidden" NAME="risco" VALUE="<?php echo $objetorisco['id_risco']?>">
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="associar<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Associar Mitigações</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/ObjetosRiscosMitigacoes/Associar/<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Objeto/Risco:</label>
                      <?php foreach ($lista_de_objetos as $objeto): ?>
                          <?php foreach ($lista_de_riscos as $risco): ?>
                            <?php if (($objetorisco['id_objeto'] == $objeto['id_objeto']) && ($objetorisco['id_risco'] == $risco['id_risco'])) : ?>
                                <input name="nome" type="textarea" value="<?php echo $objeto["nome"]; ?>/<?php echo $risco["nome"]; ?>" class="form-control" id="recipient-name">
                                  <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Mitigaçoes:</label></br>

                          <?php foreach ($lista_de_mitigacoes as $mitigacao): ?>
                            <input type='checkbox' name="checkbox[]" id="<?php echo $mitigacao['id_mitigacao']; ?>" value="<?php echo $mitigacao['id_mitigacao']; ?>"
                            <?php foreach ($ObjetoRiscoMitigacao as $ORM): ?>
                              <?php if (($ORM['id_mitigacao'] == $mitigacao['id_mitigacao']) && ($ORM['id_risco'] == $objetorisco['id_risco']) && ($ORM['id_objeto'] == $objetorisco['id_objeto'])): ?>
                                  checked="checked"
                              <?php endif; ?>
                            <?php endforeach; ?>
                            > <?php echo $mitigacao['nome']; ?> </input></br>
                          <?php endforeach; ?>

                    </div>
                    <INPUT TYPE="hidden" NAME="objeto" VALUE="<?php echo $objetorisco['id_objeto']?>"><INPUT TYPE="hidden" NAME="risco" VALUE="<?php echo $objetorisco['id_risco']?>">
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade" id="desassociar<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Desassociar Mitigações</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form method="POST" action="<?php echo BASE_URL; ?>/ObjetosRiscosMitigacoes/desassociar/<?php echo $objetorisco['id_risco']?><?php echo $objetorisco['id_objeto']?>">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label">Nome do Objeto/Risco:</label>
                      <?php foreach ($lista_de_objetos as $objeto): ?>
                          <?php foreach ($lista_de_riscos as $risco): ?>
                            <?php if (($objetorisco['id_objeto'] == $objeto['id_objeto']) && ($objetorisco['id_risco'] == $risco['id_risco'])) : ?>
                                <input name="nome" type="textarea" value="<?php echo $objeto["nome"]; ?>/<?php echo $risco["nome"]; ?>" class="form-control" id="recipient-name">
                                  <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </div>

                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Mitigaçoes:</label></br>

                          <?php foreach ($lista_de_mitigacoes as $mitigacao): ?>
                            <?php foreach ($ObjetoRiscoMitigacao as $ORM): ?>
                              <?php if (($ORM['id_mitigacao'] == $mitigacao['id_mitigacao']) && ($ORM['id_risco'] == $objetorisco['id_risco']) && ($ORM['id_objeto'] == $objetorisco['id_objeto'])): ?>
                                <input type='checkbox' name="checkbox[]" id="<?php echo $mitigacao['id_mitigacao']; ?>" value="<?php echo $mitigacao['id_mitigacao']; ?>"><?php echo $mitigacao['nome']; ?> </input></br>

                              <?php endif; ?>
                            <?php endforeach; ?>
                          <?php endforeach; ?>

                    </div>
                    <INPUT TYPE="hidden" NAME="objeto" VALUE="<?php echo $objetorisco['id_objeto']?>"><INPUT TYPE="hidden" NAME="risco" VALUE="<?php echo $objetorisco['id_risco']?>">
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                      <button type="submit" class="btn btn-primary">Finalizar</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>
  </main>
