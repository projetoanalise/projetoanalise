<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Paint 1</h1>
  </div>
  <br>

  <div class="container" id='paint1'>
    <div class="container">
  <div class="row align-items-start" id='linha1'>
    <div class="col-1" id='l1c1'>
      I <BR>
      m <BR>
      p <BR>
      a <BR>
      c <BR>
      t <BR>
      o <BR>
    </div>
    <div class="col-1" id='l1c2'>
      A <br>
      l <br>
      t <br>
      o <br>
    </div>
    <div class="col" id='l1c3'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 1) && ($risco['impacto']== 3)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].'" data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
          }}
      ?>
    </div>
    <div class="col" id='l1c4'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 2) && ($risco['impacto']== 3)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
      </div>
    <div class="col" id='l1c5'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 3) && ($risco['impacto']== 3)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
      </div>
  </div>
  <div class="row align-items-center" id='linha2'>
    <div class="col-1" id='l2c1'>
        <br>
      d <br>
      o <br>
        <br>
      r <br>
      i <br>
      s <br>
      c <br>
      o <br>

    </div>
    <div class="col-1" id='l2c2'>
      M <BR>
      é <BR>
      d <BR>
      i <BR>
      o <BR>
    </div>
    <div class="col" id='l2c3'>
      <?php foreach ($lista_de_riscos as $risco) {
            if (($risco['probabilidade'] == 1) && ($risco['impacto']== 2)) {
              echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
        }}
            ?>
    </div>
    <div class="col" id='l2c4'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 2) && ($risco['impacto']== 2)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
    </div>
    <div class="col" id='l2c5'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 3) && ($risco['impacto']== 2)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
      </div>
    </div>
  <div class="row align-items-end" id='linha3'>
    <div class="col-1" id='l3c1'>
    </div>
    <div class="col-1" id='l3c2'>
      B <BR>
      a <BR>
      i <BR>
      x <BR>
      o <BR>
    </div>
    <div class="col" id='l3c3'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 1) && ($risco['impacto']== 1)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
      </div>
    <div class="col" id='l3c4'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 2) && ($risco['impacto']== 1)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
    </div>
    <div class="col" id='l3c5'>
      <?php foreach ($lista_de_riscos as $risco) {
        if (($risco['probabilidade'] == 3) && ($risco['impacto']== 1)) {
          echo '<a href="#" data-toggle="popover" title="'.$risco['nome'].'" data-content="'.$risco['descricao'].' "data-trigger="focus">'.$risco['id_risco'].'</br> </a>';
    }}
        ?>
    </div>
  </div>

<div class="row align-items-end" id='linha4'>
  <div class="col-1" id='l4c1'>
  </div>
  <div class="col-1" id='l4c2'>

  </div>
  <div class="col" id='l4c3'>
    Baixa
  </div>
  <div class="col" id='l4c4'>
    Média
  </div>
  <div class="col" id='l4c5'>
    Alta
  </div>
</div>

<div class="row align-items-end" id='linha5'>
<div class="col-1" id='l5c1'>

</div>
<div class="col-1" id='l5c2'>

</div>
<div class="col" id='l5c3'>

</div>
<div class="col" id='l5c4'>
  <br>
  Probabilidade do risco
</div>
<div class="col" id='l5c5'>
</div>
</div>
</div>
  </div>

  <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

  <script>
 $(document).ready(function(){
    $('[data-toggle="popover"]').popover();
});
</script>
</main>
