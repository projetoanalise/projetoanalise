<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Entrar</title>
    <link href="<?php echo BASE_URL; ?>/Assets/css/login.css" rel="stylesheet" />
  </head>
  <body>
    <div class="login">

      <form method="POST">
          <?php if(isset($error) && !empty($error)){
            echo '<div class="warning">'.$error.'</div>';}?>
          <input type="users" name="email" placeholder="Usuario">
          <input type="password" name="senha" placeholder="Senha">
          <input type="submit" class="entrar" value="Entrar">
      </form>

    </div>
  </body>
</html>
