<?php

/*
 * Classe Model genérica. Importada por todos os models da aplicação.
 *
 */
class Model
{

	protected $db; // Guarda a conexão com o banco de dados.


	// Faz a conexão com o banco de dados de guarda na variável '$db'.
	public function __construct()
	{
		global $config;
		$this->db = new PDO("mysql:dbname=".$config['dbname'].";host=".
		$config['host'], $config['dbuser'], $config['dbpass']);
	}
}
