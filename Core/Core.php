<?php

/*
 * Classe Core genérica. É importada no primeiro carregamento da aplicação e
 * faz o particionamento da url designando controller, model e id dos dados
 * dinâmicos de determinada view.
 *
 */
class Core
{

	/*
   * Extrai da URL o controller, o model e um possível ID.
	 * EX: http://www.meusite.com.br/controller/model/id
   *
   */
	public function run()
	{
    $url = explode('index.php', $_SERVER['PHP_SELF']);
    $url = end($url);

		$params = array();
		if(!empty($url) && $url != '/') {
			$url = explode('/', $url);
			array_shift($url);

			$currentController = $url[0].'Controller';
			array_shift($url);

			if(isset($url[0])) {
				$currentAction = $url[0];
				array_shift($url);
			} else {
				$currentAction = 'index';
			}

			if(count($url) > 0) {
				$params = $url;
			}

		} else {
			$currentController = 'HomeController';
			$currentAction = 'index';
		}

		$currentController = ucfirst($currentController);

		$c = new $currentController();
		call_user_func_array(array($c, $currentAction), $params);
	}
}
