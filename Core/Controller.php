<?php

/*
 * Classe genérica importadas pelos controllers.
 *
 * #Conexão ao banco de dados (construtor).
 * #loadView(Carrega uma view específica).
 * #loadTemplate(Carrega o template da aplicação).
 * #loadViewInTemplate(Carrega uma view dentro do template).
 * #estaLogado(Verifica se há uma sessão aberta).
 *
 */
class Controller
{

	protected $db; // Guarda a conexão com o banco de dados.


	// Faz a conexão com o banco de dados e guarda no variável '$db'.
	public function __construct()
	{
		global $config;
		$this->db = new PDO("mysql:dbname=".$config['dbname'].";
		host=".$config['host'], $config['dbuser'], $config['dbpass']);
	}


	/*
   * Carrega uma dada view com o passando passando os dados em forma
	 * de variáveis e não mais de vetor.
   */
	public function loadView($viewName, $viewData = array())
	{
		extract($viewData);
		include 'Views/'.$viewName.'.php';
	}


	/*
   * Carrega o template(A 'carcaça' do sistema, componentes que
	 * aparecem em todas as telas[menos no login]).
	 * Ex: sidebar e navbar.
   */
	public function loadTemplate($viewName, $viewData = array())
	{
		include 'Views/template.php';
	}


	// Carrega uma view específica dentro do template.
	public function loadViewInTemplate($viewName, $viewData)
	{
		extract($viewData);
		include 'Views/'.$viewName.'.php';
	}


	/*
   * Verifica se há uma sessão logada, se não houver o usuário é
	 * redirecionado para a página de login.
   */
	 public function estaLogado()
	 {
		 $usuario = new Usuarios();
     if ($usuario->logado() == false) {
       header("Location: ".BASE_URL."/login");
       exit;
     }
	 }
}
