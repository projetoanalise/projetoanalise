


  ESSE ARQUIVO BUSCA AUXILIAR A EQUIPE DE DESENVOLVIMENTO QUANTO A UTILIZAÇÃO
  DO VERSIONAMENTO DE CÓDIGO UTILIZANDO O GIT. AQUI HÁ OS COMANDOS BÁSICOS COM
  UMA BREVE DESCRIÇÃO DO QUE ESTA FEZ. A ALTERAÇÃO DESSE ARQUIVO É PERMITIDA.



---------------------------------------------


--- CONFIGURAÇÕES INICIAIS ---

Configurar nome
#git config --global user.name "meunome"

Configurar email
#git config --global user.email "meuemail"

Configurar editor
#git config --global core.editor nomedoeditor

Ver informações cadastradas
#git config --list

---------------------------------------------

--- INICIANDO UM REPOSITÓRIO ---

Criar pasta
#mkdir nomepasta

Entrar na pasta
#cd nomepasta

Inicializar o repositório (estando na pasta)
#git init

---------------------------------------------

--- CONCEITOS ---

Branch: versões do sistema
Master: versão principal do sistema
Commit: enviar alterações pro git
README: arquivo com informações do projeto

untracked: arquivos que não estão sendo monitorados pelo git [
---------------------------------------------

--- COMANDOS GERAIS ---

Ver alterações feitas
#git status

Adicionar o arquivo ao git
#git add nomedoarquivo.extensao

Adicionar todos os arquivos untracked ao git
#git add -A

Commitar
#git commit -m "mensagem"

Dica: Adicionar e commitar ao mesmo tempo
#git commit -am "mensagem"

Ver lista de commits no branch
#git log

Listar branches do projeto ( * = branch atual )
#git branch

Voltar para um commit (estado) específico [REVERTANDO MODIFICAÇÕES]

Diferenças entre os resets
.soft: volta pro commit mas com as alterações que foram retiradas já prontas
para serem comitadas
.mixed: faz a mesma coisa do soft só que não deixa as alterações prontas pra
commit, você teria que usar o "git add" antes
.hard: deleta totalmente o commit

#git reset --soft/mixed/hard hashdocommit
[é a hash do commit para o qual você deseja voltar, e não a hash do commit que
você deseja deletar]

Criar branch
#git branch nome
ou
Cria e entra
#git checkout -b nome

Ver branches
#git branch

Apagar branch
#git branch -D nome

Trocar de branch
#git checkout nome.do.branch.para.o.qual.deseja.ir

Visualizar alterações feitas de todos os arquivos do código
#git diff

Visualizar apenas o nome dos arquivos que foram modificados
#git diff --name-only

Visualizar o que foi alterado em um arquivo específico
#git diff nomedoarquivo.extensao

Cancelar modificações de um arquivo específico
#git checkout HEAD -- nomedoarquivo.extensao

Renomear repositório remoto
#git remote rename origin curso-git

--- CONECTAR REPOSITÓRIO LOCAL AO REMOTO (GITHUB)---

No terminal:

#ssh-keygen -t rsa -b 4096 -C "your_email@example.com"

Chaves de acesso: home/you/.ssh/id_rsa

Pegar o conteúdo do arquivo da chave pública, abrir o github -> settings ->
SSH and GPG keys -> colar o conteúdo e dar um título a chave.


Conectar repositórios
#git remote add origin link

Enviar tudo pro repositório remoto:
#git push -u origin branch.para.onde.quero.enviar

Dar pull na branch de outra pessoa
#git checkout --track origin/nome-da-branch-do-brother

git push -u origin --all


--- GITIGNORE ---

Criar um arquivo .gitignore dentro do diretório e colocar os arquivos que não
devem ser commitados para o repositório remoto;
ex: arquivo.extensao
ex: extensao (não envie todos os arquivos com essa extensao)
ex: pasta/

--- REVERT ---

Você deleta alterações sem perder o código de modificação.

#git log
.pega  a hash do commit com o código de alterações
#git revert --no-edit hash

Basicamente cria um commit sem as alterações feitas, porém o commit anterior
(o que contém as alterações que foram tiradas com o git revert) ainda existe
e se tem acesso a ele para, por exemplo, ser analisado.

--- DELETANDO BRANCHES LOCAIS E REMOTOS ---

Remover branch remoto
#c

Remover branch local [precisa estar fora dele]
#git branch -D nome.do.branch

--- PUXANDO ALTERAÇÕES DE OUTRAS PESSOAS (pull) ---

É atualizar o repositório local com o remoto, dado que o remoto está com
alterações que ainda não foram inseridas no local. (é o contrário do push)

#git pull origin branch (ex: master)

Dica: antes de dar um push sempre dê um pull.

--- CLONANDO REPOSITÓRIOS REMOTOS ---

Criar um repositório local com um projeto do github.
.Estando no repositório vazio criado, copie o link do repositório do github
que deseja clonar e no terminal:
#git clone url/link.do.projeto

--- CONTRIBUINDO COM OUTROS REPOSITÓRIOS ---

Fork / pull request

Fork: pega um projeto de outra pessoa para então clonar pra fazer alterações.

Ver o nome do servidor remoto
#git remote -v

Daí após fazer a alteração, commitar e pushar o crone do projeto da outra
pessoa; no github do projeto da outra pessoa clicar em new pull request,
depois clicar em Create Pull Request.


-----------------------------------------------

No caso deu querer clonar um projeto de alguém e querer subir ele pra um
repositório meu:

Remover a conexão com o repositório original
#git remote remove origin

Adicionar conexão com outro repositório
#git remote add origin git://suaUrl

Depois de alterar
#git remote set-url origin git://suaUrl


-----------------------------------------------

--- VISUALIZANDO LOGS ---

Mostrar informações dos commits
#git log

-Filtrar por autor
 #git log --author="nome"

-Mostrar altores, commits que fizeram e quantos commits fizeram
 #git shortlog

-Ver nomes das pessoas e quantos commits fizeram
 #git shortlog -sn

-Ver de forma gráfica o que ta havendo com os branches
 #git log --graph

-Ver informações detalhadas de um commit específico
 #git show hash.do.commit

-------------------------------------------

--- MERGE ---

Estando no branch master
#git merge outrabranch


--- Git Stash --

EXEMPLO DE USO: quando eu quero dar um git pull mas ainda tenho coisas pra 
modificar.

Guardar uma modificação adicionada [esconde a modificação adicionada]
#git stash

Voltar modificação
#git stash apply

Mostrar lista de stash
#git stash list

Limpar stash
#git stash clear
