<?php

/*
 * Classe model de usuários.
 *
 * #Verifica se estáLogado
 * #Faz o login
 * #Faz o logout
 */
class Usuarios extends Model
{

  /*
   * Verifica se há uma sessão ativa, se não houver o usuário é redirecionado
   * para a página de login.
   */
  public function logado()
  {
    if(isset($_SESSION['id']) && !empty($_SESSION['id'])) {
			return true;
		} else {
			return false;
		}
  }


  /*
   * Recebe o email e a senha do usuário, faz uma query ao banco com esses
   * dados. Se o número de retornos for maior que zero, isto é, se existir uma
   * tupla com esses dados, o usuário existe e seu id é setado como variável da
   * sessão, se não retorna-se falso.
   */
  public function login($email, $senha)
  {
  		$sql = $this->db->prepare("SELECT * FROM usuarios
                                 WHERE email = :email
                                 AND senha = :senha");
  		$sql->bindValue(':email', $email);
  		$sql->bindValue(':senha', md5($senha));
  		$sql->execute();

  		if($sql->rowCount() > 0) {
  			$row = $sql->fetch();
  			$_SESSION['id'] = $row['id_usuario'];
  			return true;
  		} else {
  			return false;
  		}
	}


  public function temPermissao($id_usuario) {
    $dados_usuario = null;
    $sql = $this->db->prepare("SELECT cargo FROM usuarios WHERE id_usuario = :id_usuario");
    $sql->bindValue(':id_usuario', $id_usuario);
    $sql->execute();
    if($sql->rowCount() > 0) {
      $dados_usuario = $sql->fetch();
    }
    return $dados_usuario['cargo'];
  }


  // Destrói a sessão com ativa.
  public function logout()
  {
    unset($_SESSION['id']);
  }

}
