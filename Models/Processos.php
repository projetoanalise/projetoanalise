<?php
class Processos extends Model
{


  /*
   * Faz um query pedindo todos os processos cadastrados no banco, se o retorno
   * for maior que zero coloca todas as tuplas no vetor 'processos'.
   *
   */
  public function getProcessos($processos = array())
  {
    $sql = $this->db->prepare("SELECT * FROM processos");
    $sql->execute();

    if($sql->rowCount() > 0) {
      $processos = $sql->fetchAll();
    }
    return $processos;
  }


  /*
   * Adiciona um novo processo.
   *
   * Recebe um nome e uma descrição como parâmetro e usa estes na insersão.
   *
   */
   public function inserir($nome, $descricao)
   {
     $sql = $this->db->prepare("INSERT INTO processos
                                SET nome = :nome, descricao = :descricao");
     $sql->bindValue(':nome', $nome);
     $sql->bindValue(':descricao', $descricao);
     $sql->execute();
   }


   /*
    * Edita um processo.
    *
    * Recebe o id do processo, seu nome e descrição e usa estes a alteração.
    *
    */
    public function editar($id_processo, $nome, $descricao)
    {
      $sql = $this->db->prepare("UPDATE processos
                                 SET nome = :nome, descricao = :descricao
                                 WHERE id_processo = $id_processo");
      $sql->bindValue(':nome', $nome);
      $sql->bindValue(':descricao', $descricao);
      $sql->execute();
    }


    /*
     * Recebe o id do processo que deseja excluir depois faz a exclusão.
     *
     *
     */
     public function excluir($id_processo)
     {
       $sql = $this->db->prepare("DELETE FROM processos
                                  WHERE id_processo = :id_processo");
       $sql->bindValue(':id_processo', $id_processo);
       $sql->execute();
     }
}
