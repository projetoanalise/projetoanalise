<?php

/*
 * Classe model de riscos.
 * #Inserir
 * #Editar
 * #Excluir
 * #Listar informações
 * #Listar todos os riscos
 * #Listar todos os objeto_riscos
 * #Excluir um objeto_risco
 * #Associar um objeto_risco
 *
 */
class Riscos extends Model
{

  // Recebe nome, descrição, impacto e probabilidade de um risco e o insere.
  public function inserir($nome, $descricao, $impacto, $probabilidade)
  {
    $sql = $this->db->prepare("INSERT INTO riscos SET resultado = :resultado,
                               nome = :nome, descricao = :descricao, impacto =
                               :impacto, probabilidade = :probabilidade");
    $sql->bindValue(':nome', $nome);
    $sql->bindValue(':impacto', $impacto);
    $sql->bindValue(':probabilidade', $probabilidade);
    $sql->bindValue(':descricao', $descricao);
    $sql->bindValue(':resultado', ($impacto+$probabilidade));
    $sql->execute();
  }


  /* Recebe id, nome, descricao, impacto e probabilidade de um risco e usando
     esses dados edita os dados. */
  public function editar($id_risco, $nome, $descricao, $impacto, $probabilidade)
  {
    $sql = $this->db->prepare("UPDATE riscos SET resultado = :resultado, nome =
                               :nome, descricao = :descricao, impacto =
                               :impacto, probabilidade = :probabilidade
                               WHERE id_risco = $id_risco");
    $sql->bindValue(':nome', $nome);
    $sql->bindValue(':impacto', $impacto);
    $sql->bindValue(':probabilidade', $probabilidade);
    $sql->bindValue(':descricao', $descricao);
    $sql->bindValue(':resultado', ($impacto+$probabilidade));
    $sql->execute();
  }


  // Recebe um id e exclui o risco referente a esse id.
  public function excluir($id_risco)
  {
    $sql = $this->db->prepare("DELETE FROM riscos WHERE id_risco = :id_risco");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->execute();
  }


  // Recebe um id e lista as informações do risco referente a esse id.
  public function listarUmRisco($id_risco)
  {
    $sql = $this->db->prepare("SELECT * FROM riscosWHERE id_risco = :id_risco");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->execute();

    if($sql->rowCount() > 0) {
      $risco = $sql->fetchAll();
    }
    return $risco;
  }


  // Recebe um vetor de riscos e lista todos estes.
  public function listarRiscos($riscos = array())
  {
    $sql = $this->db->prepare("SELECT * FROM riscos");
    $sql->execute();

    if($sql->rowCount() > 0) {
      $riscos = $sql->fetchAll();
    }
    return $riscos;
  }


  // Lista todos os objeto_riscos.
  public function listarObjetosRiscos($riscos = array())
  {
    $sql = $this->db->prepare("SELECT * FROM objeto_risco
                               WHERE id_risco = :id_risco");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->execute();

    if($sql->rowCount() > 0) {
      $ObjetosRiscos = $sql->fetchAll();
    }
    return $ObjetosRiscos;
  }


  // Recebe os ids de um objeto e de um risco e exclui o registro referente.
  public function excluirObjetoRisco($id_objeto, $id_risco)
  {
    $sql = $this->db->prepare("DELETE FROM objeto_risco
                               WHERE id_risco = :id_risco
                               AND id_objeto = :id_objeto");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->bindValue(':id_objeto', $id_objeto);
    $sql->execute();
  }


  // Recebe os ids de um objeto e de um risco e associa o registro referente.
  public function associarObjetosRiscos($id_objeto, $id_risco)
  {
    $sql = $this->db->prepare("INSERT INTO objeto_risco
                               SET id_objeto = :id_objeto,
                                   id_risco = :id_risco");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->bindValue(':id_objeto', $id_objeto);
    $sql->execute();
  }

}
