<?php

/**
 * Classe de CRUD de mitigacoes.
 *
 * #Inserir
 * #Editar
 * #Listar informações
 * #Excluir
 * #Listar todas as mitigações
 */
class Mitigacoes extends Model
{

  // Recebe um nome e uma descrição de determinada mitigação e a insere.
  public function inserir($nome, $descricao)
  {
    $sql = $this->db->prepare("INSERT INTO mitigacoes
                               SET nome = :nome, descricao = :descricao");
    $sql->bindValue(':nome', $nome);
    $sql->bindValue(':descricao', $descricao);
    $sql->execute();
  }


  // Recebe um id, um nome e uma descrição de uma mitigação e atualiza a mesma.
  public function editar($id_mitigacao, $nome, $descricao)
  {
    $sql = $this->db->prepare("UPDATE mitigacoes
                               SET nome = :nome, descricao = :descricao
                               WHERE id_mitigacao = $id_mitigacao");
    $sql->bindValue(':nome', $nome);
    $sql->bindValue(':descricao', $descricao);
    $sql->execute();
  }


  // Recebe um id de uma determinada mitigação e lista suas informações.
  public function listarUmaMitigacao($id_mitigacao)
  {
    $sql = $this->db->prepare("SELECT * FROM mitigacoes
                                WHERE id_mitigacao = :id_mitigacao");
    $sql->bindValue(':id_mitigacao', $id_mitigacao);
    $sql->execute();

    if($sql->rowCount() > 0) {
      $mitigacao = $sql->fetchAll();
    }
    return $mitigacao;
  }

  // Recebe um id de uma determinada mitigação e usando esse id a exclui.
  public function excluir($id_mitigacao)
  {
    $sql = $this->db->prepare("DELETE FROM mitigacoes
                               WHERE id_mitigacao = :id_mitigacao");
    $sql->bindValue(':id_mitigacao', $id_mitigacao);
    $sql->execute();
  }


  /* Recebe um vetor onde será guardado todas as mitigações e lista suas
     informações. */
  public function getMitigacoes($mitigacoes = array())
  {
    $sql = $this->db->prepare("SELECT * FROM mitigacoes");
    $sql->execute();

    if($sql->rowCount() > 0) {
      $mitigacoes = $sql->fetchAll();
    }
    return $mitigacoes;
  }
}
