<?php

/**
 * Classe de CRUD de mitigacoes.
 *
 * #Inserir
 * #Editar
 * #Listar informações
 * #Excluir
 * #Listar todas as mitigações
 */
class Objetos extends Model
{

  // Recebe um nome, uma descrição e um id e com isso adiciona um novo objeto.
  public function novoObjeto($nome, $descricao, $id_processo)
  {
    $sql = $this->db->prepare("INSERT INTO objetos
                               SET nome = :nome, descricao = :descricao,
                                   id_processo = :id_processo");
    $sql->bindValue(':nome', $nome);
    $sql->bindValue(':descricao', $descricao);
    $sql->bindValue(':id_processo', $id_processo);
    $sql->execute();
  }


  // Recebe um vetor onde será guardado os objetos e os lista.
  public function getObjetos($objetos = array())
  {
   $sql = $this->db->prepare("SELECT * FROM objetos");
   $sql->execute();

   if($sql->rowCount() > 0) {
     $objetos = $sql->fetchAll();
   }
   return $objetos;
  }


  /* Recebe um vetor onde será armazenado registros da tabela 'objeto_risco' e
    os lista. */
  public function listarObjetosRiscos($objeto_risco = array())
  {
   $sql = $this->db->prepare("SELECT * FROM objeto_risco");
   $sql->execute();

   if($sql->rowCount() > 0) {
     $objetos_riscos = $sql->fetchAll();
   }
   return $objetos_riscos;
  }


  // Recebe o id de um objeto e, com isso, lista suas informações.
  public function listarUmObjeto($id_objeto)
  {
   $sql = $this->db->prepare("SELECT * FROM objeto WHERE id_objeto = :id_objeto");
   $sql->bindValue(':id_objeto', $id_objeto);
   $sql->execute();

   if($sql->rowCount() > 0) {
     $objeto = $sql->fetchAll();
   }
   return $objeto;
  }
}
