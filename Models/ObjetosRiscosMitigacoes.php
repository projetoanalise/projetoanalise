<?php

/**
 * Classe de CRUD de mitigacoes.
 *
 * #Inserir
 * #Editar
 * #Listar informações
 * #Excluir
 * #Listar todas as mitigações
 */
class ObjetosRiscosMitigacoes extends Model
{


  //
  // public function getObjetosRiscosMitigacoes($dados = array())
  // {
  //   $sql = $this->db->prepare("UPDATE objeto_risco SET resultado = :resultado, nome = :nome, descricao = :descricao, impacto = :impacto, probabilidade = :probabilidade WHERE id_risco = $id_risco");
  //   $sql->bindValue(':nome', $nome);
  //   $sql->bindValue(':impacto', $impacto);
  //   $sql->bindValue(':probabilidade', $probabilidade);
  //   $sql->bindValue(':descricao', $descricao);
  //   $sql->bindValue(':resultado', ($impacto+$probabilidade));
  //   $sql->execute();
  //
  //   if($sql->rowCount() > 0) {
  //     $dados = $sql->fetchAll();
  //   }
  //   return $dados;
  // }

  public function getObjetosRiscosMitigacoes($dados = array())
  {
    $sql = $this->db->prepare("SELECT * FROM objeto_risco_mitigacao");
    $sql->execute();

    if($sql->rowCount() > 0) {
      $dados = $sql->fetchAll();
    }
    return $dados;
  }


  //
  public function listarObjetosRiscos($dados = array())
  {
    $sql = $this->db->prepare("SELECT * FROM objeto_risco");
    $sql->execute();

    if($sql->rowCount() > 0) {
      $dados = $sql->fetchAll();
    }
    return $dados;
  }


  //
  public function excluirObjetoRisco($id_risco, $id_objeto)
  {
    $sql = $this->db->prepare("DELETE FROM objeto_risco WHERE id_objeto = :id_objeto AND id_risco = :id_risco");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->bindValue(':id_objeto', $id_objeto);
    $sql->execute();
  }


  /* Recebe os valores de avaliação, um id de um risco e um id de um objeto e
     edita o registro referente a este id com os valores recebidos. */
  public function avaliar($classificacao, $risco, $objeto)
  {
    $sql = $this->db->prepare("UPDATE objeto_risco
                               SET avaliacao_mitigacao = :avaliacao_mitigacao
                               WHERE id_risco = $risco
                               AND id_objeto = $objeto");
    $sql->bindValue(':avaliacao_mitigacao', $classificacao);
    $sql->execute();
  }


  /* Recebe os ientificadores de um objeto, um risco e uma mitigação e
     associa o registro referente a esses 3 valores a um determinado
     processo, ou seja. insere na tabela objeto_risco_mitigacao. */
  public function associarObjetosRiscosMitigacao($id_objeto, $id_risco,
  $id_mitigacao)
  {
    $sql = $this->db->prepare("INSERT INTO objeto_risco_mitigacao
                               SET id_objeto = :id_objeto ,id_risco = :id_risco,
                               id_mitigacao = :id_mitigacao");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->bindValue(':id_objeto', $id_objeto);
    $sql->bindValue(':id_mitigacao', $id_mitigacao);
    $sql->execute();
  }


  /* Recebe os ientificadores de um objeto, um risco e uma mitigação e
     desassocia o registro referente a esses 3 valores de um determinado
     processo, ou seja. exclui da tabela objeto_risco_mitigacao. */
  public function desassociarObjetosRiscosMitigacao($id_objeto, $id_risco,
  $id_mitigacao)
  {
    $sql = $this->db->prepare("DELETE FROM objeto_risco_mitigacao
                               WHERE id_objeto = :id_objeto
                               AND id_risco = :id_risco
                               AND id_mitigacao = :id_mitigacao");
    $sql->bindValue(':id_risco', $id_risco);
    $sql->bindValue(':id_objeto', $id_objeto);
    $sql->bindValue(':id_mitigacao', $id_mitigacao);
    $sql->execute();
  }
}
